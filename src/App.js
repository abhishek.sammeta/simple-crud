import { useEffect, useState } from 'react';
import mockApi from './mockApi';
import UsersList from './components/user-wiz/UsersList';
import ModalFormComponent from './components/user-wiz/modal-form';
import { message } from 'antd';

function App() {

  const [users, setUsers] = useState([]);
  const [showEditUserModal, setShowEditUserModal] = useState(false);
  const [selectedUser, setSelectedUser] = useState({});
  const [mode, setMode] = useState('add');

  useEffect(async () => {
    try {
      getAllUsers();
    } catch (err) {
      console.error("error while getting users", err)
    }
  }, []);

  // get all 
  const getAllUsers = async () => {
    await mockApi.getAll().then((response) => {
      setUsers(response.data);
    })
  }

  // add or update
  const handleUser = () => {
    switch (mode) {
      case 'add':
        addUser();
        break;
      case 'edit':
        editUser();
        break;
    }
  }

  // validate input feilds
  const validateInputs = () => {
    return Object.keys(selectedUser).some((key) =>
      selectedUser[key].trim() !== '' && Object.keys(selectedUser).length >= 3
    )
  }

  // add user
  const handleAddUser = async () => {
    setMode('add');
    setSelectedUser({});
    setShowEditUserModal(true);
  }

  const addUser = async () => {
    if (validateInputs()) {
      // await mockApi.create(selectedUser).then((response) => {
      //   getAllUsers();
      //   setShowEditUserModal(false);
      // })
    } else {
      message.warning("Please all feilds")
    }
  }

  // edit user
  const handleEditUser = async (id) => {
    let user = users.filter((value) => id === value.id)
    setMode('edit');
    setSelectedUser(user[0]);
    setShowEditUserModal(true);
  }

  const editUser = async () => {
    if (validateInputs()) {
      await mockApi.update(selectedUser.id, selectedUser).then((response) => {
        getAllUsers();
        setShowEditUserModal(false);
      })
    } else {
      message.warning("Please all feilds")
    }
  }

  const cancelEditModal = async () => {
    setSelectedUser({});
    setShowEditUserModal(false);
  }

  // generic onchange function for all input feilds
  const handleOnchange = (e) => {
    let data = { ...selectedUser };
    data[e.target.name] = e.target.value;
    setSelectedUser(data);
  }

  // delete 
  const deleteUser = async (id) => {
    await mockApi.delete(id).then((response) => {
      message.success("User deleted")
      getAllUsers();
    });
  }

  return <>
  {/* Table display of users */}
    <UsersList
      dataSource={users}
      handleAddUser={handleAddUser}
      addUser={addUser}
      handleEditUser={handleEditUser}
      edit={editUser}
      delete={deleteUser}
    />
    {/* Form to add or edit user */}
    <ModalFormComponent
      mode={mode}
      visible={showEditUserModal}
      onCancel={cancelEditModal}
      onOk={handleUser}
      handleOnchange={handleOnchange}
      selectedUser={selectedUser}
    />
  </>
}

export default App;
