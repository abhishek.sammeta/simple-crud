import axios from "axios";

export default {

    execute: async (path, method, body) => {

        return axios({
            headers: {"Content-Type": "application/json"},
            url: path,
            method: method,
            data: body,
        });
    }
};
