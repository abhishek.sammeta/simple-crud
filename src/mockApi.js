import config from "./utils/config";
import server from "./utils/server";
const prefixPath = `${config.server.server_url}application`;

// handles all api's
const mockApi = {
    getAll: async () => {
        return await server.execute(prefixPath, 'GET');
    },

    get: async (id) => {
        return await server.execute(`${prefixPath}/${id}`, 'GET');
    },

    update: async (id, body) => {
        return await server.execute(`${prefixPath}/${id}`, 'PUT', body);
    },

    create: async (body) => {
        return await server.execute(prefixPath, 'POST', body);
    },

    delete: async (id) => {
        return await server.execute(`${prefixPath}/${id}`, 'DELETE');
    }
}

export default mockApi;