import React from "react";
import { Modal, Input } from "antd";

const ModalFormComponent = (props) => {

  return (
    <Modal
        visible={props.visible}
        title={props.mode === 'add' ? "Add Applicant" : "Edit Applicant"}
        okText={props.mode === 'add' ? "Add" : "Update" }
        onCancel={props.onCancel}
        onOk={props.onOk}
    >
        <>
            Name
            <Input 
                id='Name'
                name='name'
                value={props.selectedUser.name}
                onChange={props.handleOnchange}
            ></Input>
            Email
            <Input 
                id='email'
                name='email'
                value={props.selectedUser.email}
                onChange={props.handleOnchange}
            ></Input>
            Resume URL
            <Input 
                id='resumeUrl'
                name='resumeUrl'
                value={props.selectedUser.resumeUrl}
                onChange={props.handleOnchange}
            ></Input>
        </>
    </Modal>
  );
}

export default ModalFormComponent;