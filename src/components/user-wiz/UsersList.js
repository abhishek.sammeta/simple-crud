import { Table, Row, Col, Tooltip, Button, Popconfirm } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';

// Table to display a collection of structured data
function UsersList(props) {

  const tableColumns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Resume URL',
      dataIndex: 'resumeUrl',
      key: 'resumeUrl',
    },
    {
      title: 'Action',
      dataIndex: 'id',
      render: (id) => (<Row style={{ fontSize: '20px', textAlign: 'center' }}>
        <Col md={12} >
          <Tooltip title='Edit' >
            <EditOutlined
              onClick={() => props.handleEditUser(id)}
            />
          </Tooltip>
        </Col>
        <Col md={12} >
          <Popconfirm
            title="Are you sure to delete this applicant?"
            onConfirm={() => props.delete(id)}
            okText="Yes"
            cancelText="No"
            placement="topRight"
          >
            <DeleteOutlined />
          </Popconfirm>
        </Col>
      </Row>)
    },
  ];
  return <Table
    dataSource={props.dataSource}
    columns={tableColumns}
    bordered
    title={() => (<Row>
      <Col md={4}>Applicants</Col>
      <Col md={16}></Col>
      <Col md={4}> <Button type='primary' onClick={props.handleAddUser} >Add Applicant</Button> </Col>
    </Row>)}
  />
}

export default UsersList;